using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PlayFab.ServerModels
{
	
	
	
	public class AddCharacterVirtualCurrencyRequest
	{
		
		
		/// <summary>
		/// PlayFab unique identifier of the user whose virtual currency balance is to be incremented.
		/// </summary>
		public string PlayFabId { get; set;}
		
		public string CharacterId { get; set;}
		
		/// <summary>
		/// Name of the virtual currency which is to be incremented.
		/// </summary>
		public string VirtualCurrency { get; set;}
		
		/// <summary>
		/// Amount to be added to the user balance of the specified virtual currency.
		/// </summary>
		public int Amount { get; set;}
		
		
	}
	
	
	
	public class AddSharedGroupMembersRequest
	{
		
		
		/// <summary>
		/// Unique identifier for the shared group.
		/// </summary>
		public string SharedGroupId { get; set;}
		
		public List<string> PlayFabIds { get; set;}
		
		
	}
	
	
	
	public class AddSharedGroupMembersResult
	{
		
		
		
	}
	
	
	
	public class AddUserVirtualCurrencyRequest
	{
		
		
		/// <summary>
		/// PlayFab unique identifier of the user whose virtual currency balance is to be increased.
		/// </summary>
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// Name of the virtual currency which is to be incremented.
		/// </summary>
		public string VirtualCurrency { get; set;}
		
		/// <summary>
		/// Amount to be added to the user balance of the specified virtual currency.
		/// </summary>
		public int Amount { get; set;}
		
		
	}
	
	
	
	public class AuthenticateSessionTicketRequest
	{
		
		
		/// <summary>
		/// Session ticket as issued by a PlayFab client login API.
		/// </summary>
		public string SessionTicket { get; set;}
		
		
	}
	
	
	
	public class AuthenticateSessionTicketResult
	{
		
		
		/// <summary>
		/// Account info for the user whose session ticket was supplied.
		/// </summary>
		public UserAccountInfo UserInfo { get; set;}
		
		
	}
	
	
	
	public class AwardSteamAchievementItem
	{
		
		
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// Unique Steam achievement name.
		/// </summary>
		public string AchievementName { get; set;}
		
		/// <summary>
		/// Result of the award attempt (only valid on response, not on request).
		/// </summary>
		public bool Result { get; set;}
		
		
	}
	
	
	
	public class AwardSteamAchievementRequest
	{
		
		
		/// <summary>
		/// Array of achievements to grant and the users to whom they are to be granted.
		/// </summary>
		public List<AwardSteamAchievementItem> Achievements { get; set;}
		
		
	}
	
	
	
	public class AwardSteamAchievementResult
	{
		
		
		/// <summary>
		/// Array of achievements granted.
		/// </summary>
		public List<AwardSteamAchievementItem> AchievementResults { get; set;}
		
		
	}
	
	
	
	/// <summary>
	/// A purchasable item from the item catalog
	/// </summary>
	public class CatalogItem : IComparable<CatalogItem>
	{
		
		
		/// <summary>
		/// unique identifier for this item
		/// </summary>
		public string ItemId { get; set;}
		
		/// <summary>
		/// class to which the item belongs
		/// </summary>
		public string ItemClass { get; set;}
		
		/// <summary>
		/// catalog item for this item
		/// </summary>
		public string CatalogVersion { get; set;}
		
		/// <summary>
		/// text name for the item, to show in-game
		/// </summary>
		public string DisplayName { get; set;}
		
		/// <summary>
		/// text description of item, to show in-game
		/// </summary>
		public string Description { get; set;}
		
		/// <summary>
		/// price of this item in virtual currencies and "RM" (the base Real Money purchase price, in USD pennies)
		/// </summary>
		public Dictionary<string,uint> VirtualCurrencyPrices { get; set;}
		
		/// <summary>
		/// override prices for this item for specific currencies
		/// </summary>
		public Dictionary<string,uint> RealCurrencyPrices { get; set;}
		
		/// <summary>
		/// list of item tags
		/// </summary>
		[Unordered]
		public List<string> Tags { get; set;}
		
		/// <summary>
		/// game specific custom data
		/// </summary>
		public string CustomData { get; set;}
		
		/// <summary>
		/// array of ItemId values which are evaluated when any item is added to the player inventory - if all items in this array are present, the this item will also be added to the player inventory
		/// </summary>
		[Unordered]
		public List<string> GrantedIfPlayerHas { get; set;}
		
		/// <summary>
		/// defines the consumable properties (number of uses, timeout) for the item
		/// </summary>
		public CatalogItemConsumableInfo Consumable { get; set;}
		
		/// <summary>
		/// defines the container properties for the item - what items it contains, including random drop tables and virtual currencies, and what item (if any) is required to open it via the UnlockContainerItem API
		/// </summary>
		public CatalogItemContainerInfo Container { get; set;}
		
		/// <summary>
		/// defines the bundle properties for the item - bundles are items which contain other items, including random drop tables and virtual currencies
		/// </summary>
		public CatalogItemBundleInfo Bundle { get; set;}
		
		/// <summary>
		/// if true, then this item instance can be used to grant a character to a user.
		/// </summary>
		public bool CanBecomeCharacter { get; set;}
		
		/// <summary>
		/// if true, then only one item instance of this type will exist and its remaininguses will be incremented instead
		/// </summary>
		public bool IsStackable { get; set;}
		
		
		public int CompareTo(CatalogItem other)
        {
            if (other == null || other.ItemId == null) return 1;
            if (ItemId == null) return -1;
            return ItemId.CompareTo(other.ItemId);
        }
		
	}
	
	
	
	public class CatalogItemBundleInfo
	{
		
		
		/// <summary>
		/// unique ItemId values for all items which will be added to the player inventory when the bundle is added
		/// </summary>
		[Unordered]
		public List<string> BundledItems { get; set;}
		
		/// <summary>
		/// unique TableId values for all RandomResultTable objects which are part of the bundle (random tables will be resolved and add the relevant items to the player inventory when the bundle is added)
		/// </summary>
		[Unordered]
		public List<string> BundledResultTables { get; set;}
		
		/// <summary>
		/// virtual currency types and balances which will be added to the player inventory when the bundle is added
		/// </summary>
		public Dictionary<string,uint> BundledVirtualCurrencies { get; set;}
		
		
	}
	
	
	
	public class CatalogItemConsumableInfo
	{
		
		
		/// <summary>
		/// number of times this object can be used, after which it will be removed from the player inventory
		/// </summary>
		public uint? UsageCount { get; set;}
		
		/// <summary>
		/// duration in seconds for how long the item will remain in the player inventory - once elapsed, the item will be removed
		/// </summary>
		public uint? UsagePeriod { get; set;}
		
		/// <summary>
		/// all inventory item instances in the player inventory sharing a non-null UsagePeriodGroup have their UsagePeriod values added together, and share the result - when that period has elapsed, all the items in the group will be removed
		/// </summary>
		public string UsagePeriodGroup { get; set;}
		
		
	}
	
	
	
	/// <summary>
	/// Containers are inventory items that can hold other items defined in the catalog, as well as virtual currency, which is added to the player inventory when the container is unlocked, using the UnlockContainerItem API. The items can be anything defined in the catalog, as well as RandomResultTable objects which will be resolved when the container is unlocked. Containers and their keys should be defined as Consumable (having a limited number of uses) in their catalog defintiions, unless the intent is for the player to be able to re-use them infinitely.
	/// </summary>
	public class CatalogItemContainerInfo
	{
		
		
		/// <summary>
		/// ItemId for the catalog item used to unlock the container, if any (if not specified, a call to UnlockContainerItem will open the container, adding the contents to the player inventory and currency balances)
		/// </summary>
		public string KeyItemId { get; set;}
		
		/// <summary>
		/// unique ItemId values for all items which will be added to the player inventory, once the container has been unlocked
		/// </summary>
		[Unordered]
		public List<string> ItemContents { get; set;}
		
		/// <summary>
		/// unique TableId values for all RandomResultTable objects which are part of the container (once unlocked, random tables will be resolved and add the relevant items to the player inventory)
		/// </summary>
		[Unordered]
		public List<string> ResultTableContents { get; set;}
		
		/// <summary>
		/// virtual currency types and balances which will be added to the player inventory when the container is unlocked
		/// </summary>
		public Dictionary<string,uint> VirtualCurrencyContents { get; set;}
		
		
	}
	
	
	
	public class CharacterLeaderboardEntry
	{
		
		
		/// <summary>
		/// PlayFab unique identifier of the user for this leaderboard entry.
		/// </summary>
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// PlayFab unique identifier of the character that belongs to the user for this leaderboard entry.
		/// </summary>
		public string CharacterId { get; set;}
		
		/// <summary>
		/// Title-specific display name of the character for this leaderboard entry.
		/// </summary>
		public string CharacterName { get; set;}
		
		/// <summary>
		/// Title-specific display name of the user for this leaderboard entry.
		/// </summary>
		public string DisplayName { get; set;}
		
		/// <summary>
		/// Name of the character class for this entry.
		/// </summary>
		public string CharacterType { get; set;}
		
		/// <summary>
		/// Specific value of the user's statistic.
		/// </summary>
		public int StatValue { get; set;}
		
		/// <summary>
		/// User's overall position in the leaderboard.
		/// </summary>
		public int Position { get; set;}
		
		
	}
	
	
	
	public class CharacterResult
	{
		
		
		public string CharacterId { get; set;}
		
		public string CharacterName { get; set;}
		
		public string CharacterType { get; set;}
		
		
	}
	
	
	
	public class CreateSharedGroupRequest
	{
		
		
		/// <summary>
		/// Unique identifier for the shared group (a random identifier will be assigned, if one is not specified).
		/// </summary>
		public string SharedGroupId { get; set;}
		
		
	}
	
	
	
	public class CreateSharedGroupResult
	{
		
		
		/// <summary>
		/// Unique identifier for the shared group.
		/// </summary>
		public string SharedGroupId { get; set;}
		
		
	}
	
	
	
	public enum Currency
	{
		USD,
		GBP,
		EUR,
		RUB,
		BRL,
		CIS,
		CAD
	}
	
	
	
	public class DeleteCharacterFromUserRequest
	{
		
		
		public string PlayFabId { get; set;}
		
		public string CharacterId { get; set;}
		
		/// <summary>
		/// If true, the character's inventory will be transferred up to the owning user; otherwise, this request will purge those items.
		/// </summary>
		public bool SaveCharacterInventory { get; set;}
		
		
	}
	
	
	
	public class DeleteCharacterFromUserResult
	{
		
		
		
	}
	
	
	
	public class DeleteSharedGroupRequest
	{
		
		
		/// <summary>
		/// Unique identifier for the shared group.
		/// </summary>
		public string SharedGroupId { get; set;}
		
		
	}
	
	
	
	public class EmptyResult
	{
		
		
		
	}
	
	
	
	public class GetCatalogItemsRequest
	{
		
		
		/// <summary>
		/// Which catalog is being requested.
		/// </summary>
		public string CatalogVersion { get; set;}
		
		
	}
	
	
	
	public class GetCatalogItemsResult
	{
		
		
		/// <summary>
		/// Array of items which can be purchased.
		/// </summary>
		[Unordered(SortProperty="ItemId")]
		public List<CatalogItem> Catalog { get; set;}
		
		
	}
	
	
	
	public class GetCharacterDataRequest
	{
		
		
		public string PlayFabId { get; set;}
		
		public string CharacterId { get; set;}
		
		/// <summary>
		/// Specific keys to search for in the custom user data.
		/// </summary>
		public List<string> Keys { get; set;}
		
		/// <summary>
		/// The version that currently exists according to the caller. The call will return the data for all of the keys if the version in the system is greater than this.
		/// </summary>
		public int? IfChangedFromDataVersion { get; set;}
		
		
	}
	
	
	
	public class GetCharacterDataResult
	{
		
		
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// Indicates the current version of the data that has been set. This is incremented with every set call for that type of data (read-only, internal, etc). This version can be provided in Get calls to find updated data.
		/// </summary>
		public uint DataVersion { get; set;}
		
		/// <summary>
		/// User specific data for this title.
		/// </summary>
		public Dictionary<string,UserDataRecord> Data { get; set;}
		
		public string CharacterId { get; set;}
		
		
	}
	
	
	
	public class GetCharacterInventoryRequest
	{
		
		
		public string PlayFabId { get; set;}
		
		public string CharacterId { get; set;}
		
		/// <summary>
		/// Used to limit results to only those from a specific catalog version.
		/// </summary>
		public string CatalogVersion { get; set;}
		
		
	}
	
	
	
	public class GetCharacterInventoryResult
	{
		
		
		/// <summary>
		/// Array of inventory items belonging to the character.
		/// </summary>
		[Unordered(SortProperty="ItemInstanceId")]
		public List<ItemInstance> Inventory { get; set;}
		
		/// <summary>
		/// Array of virtual currency balance(s) belonging to the character.
		/// </summary>
		public Dictionary<string,int> VirtualCurrency { get; set;}
		
		
	}
	
	
	
	public class GetCharacterLeaderboardRequest
	{
		
		
		public string CharacterId { get; set;}
		
		/// <summary>
		/// Optional character type on which to filter the leaderboard entries.
		/// </summary>
		public string CharacterType { get; set;}
		
		/// <summary>
		/// Unique identifier for the title-specific statistic for the leaderboard.
		/// </summary>
		public string StatisticName { get; set;}
		
		/// <summary>
		/// First entry in the leaderboard to be retrieved.
		/// </summary>
		public int StartPosition { get; set;}
		
		/// <summary>
		/// Maximum number of entries to retrieve.
		/// </summary>
		public int MaxResultsCount { get; set;}
		
		
	}
	
	
	
	public class GetCharacterLeaderboardResult
	{
		
		
		/// <summary>
		/// Ordered list of leaderboard entries.
		/// </summary>
		public List<CharacterLeaderboardEntry> Leaderboard { get; set;}
		
		
	}
	
	
	
	public class GetCharacterStatisticsRequest
	{
		
		
		public string PlayFabId { get; set;}
		
		public string CharacterId { get; set;}
		
		
	}
	
	
	
	public class GetCharacterStatisticsResult
	{
		
		
		/// <summary>
		/// Character statistics for the requested user.
		/// </summary>
		public Dictionary<string,int> CharacterStatistics { get; set;}
		
		
	}
	
	
	
	public class GetContentDownloadUrlRequest
	{
		
		
		/// <summary>
		/// Key of the content item to fetch, usually formatted as a path, e.g. images/a.png
		/// </summary>
		public string Key { get; set;}
		
		/// <summary>
		/// HTTP method to fetch item - GET or HEAD. Use HEAD when only fetching metadata. Default is GET.
		/// </summary>
		public string HttpMethod { get; set;}
		
		/// <summary>
		/// True if download through CDN. CDN provides better download bandwidth and time. However, if you want latest, non-cached version of the content, set this to false. Default is true.
		/// </summary>
		public bool? ThruCDN { get; set;}
		
		
	}
	
	
	
	public class GetContentDownloadUrlResult
	{
		
		
		/// <summary>
		/// URL for downloading content via HTTP GET or HEAD method. The URL will expire in 1 hour.
		/// </summary>
		public string URL { get; set;}
		
		
	}
	
	
	
	public class GetLeaderboardAroundCharacterRequest
	{
		
		
		/// <summary>
		/// Unique identifier for the title-specific statistic for the leaderboard.
		/// </summary>
		public string StatisticName { get; set;}
		
		public string PlayFabId { get; set;}
		
		public string CharacterId { get; set;}
		
		/// <summary>
		/// Optional character type on which to filter the leaderboard entries.
		/// </summary>
		public string CharacterType { get; set;}
		
		/// <summary>
		/// Maximum number of entries to retrieve.
		/// </summary>
		public int MaxResultsCount { get; set;}
		
		
	}
	
	
	
	public class GetLeaderboardAroundCharacterResult
	{
		
		
		/// <summary>
		/// Ordered list of leaderboard entries.
		/// </summary>
		public List<CharacterLeaderboardEntry> Leaderboard { get; set;}
		
		
	}
	
	
	
	public class GetLeaderboardAroundUserRequest
	{
		
		
		/// <summary>
		/// Unique identifier for the title-specific statistic for the leaderboard.
		/// </summary>
		public string StatisticName { get; set;}
		
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// Maximum number of entries to retrieve.
		/// </summary>
		public int MaxResultsCount { get; set;}
		
		
	}
	
	
	
	public class GetLeaderboardAroundUserResult
	{
		
		
		/// <summary>
		/// Ordered list of leaderboard entries.
		/// </summary>
		public List<PlayerLeaderboardEntry> Leaderboard { get; set;}
		
		
	}
	
	
	
	public class GetLeaderboardForUsersCharactersRequest
	{
		
		
		/// <summary>
		/// Unique identifier for the title-specific statistic for the leaderboard.
		/// </summary>
		public string StatisticName { get; set;}
		
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// Maximum number of entries to retrieve.
		/// </summary>
		public int MaxResultsCount { get; set;}
		
		
	}
	
	
	
	public class GetLeaderboardForUsersCharactersResult
	{
		
		
		/// <summary>
		/// Ordered list of leaderboard entries.
		/// </summary>
		public List<CharacterLeaderboardEntry> Leaderboard { get; set;}
		
		
	}
	
	
	
	public class GetLeaderboardRequest
	{
		
		
		/// <summary>
		/// Unique identifier for the title-specific statistic for the leaderboard.
		/// </summary>
		public string StatisticName { get; set;}
		
		/// <summary>
		/// First entry in the leaderboard to be retrieved.
		/// </summary>
		public int StartPosition { get; set;}
		
		/// <summary>
		/// Maximum number of entries to retrieve.
		/// </summary>
		public int MaxResultsCount { get; set;}
		
		
	}
	
	
	
	public class GetLeaderboardResult
	{
		
		
		/// <summary>
		/// Ordered list of leaderboard entries.
		/// </summary>
		public List<PlayerLeaderboardEntry> Leaderboard { get; set;}
		
		
	}
	
	
	
	public class GetPublisherDataRequest
	{
		
		
		/// <summary>
		///  array of keys to get back data from the Publisher data blob, set by the admin tools
		/// </summary>
		public List<string> Keys { get; set;}
		
		
	}
	
	
	
	public class GetPublisherDataResult
	{
		
		
		/// <summary>
		/// a dictionary object of key / value pairs
		/// </summary>
		public Dictionary<string,string> Data { get; set;}
		
		
	}
	
	
	
	public class GetSharedGroupDataRequest
	{
		
		
		/// <summary>
		/// Unique identifier for the shared group.
		/// </summary>
		public string SharedGroupId { get; set;}
		
		/// <summary>
		/// Specific keys to retrieve from the shared group (if not specified, all keys will be returned, while an empty array indicates that no keys should be returned).
		/// </summary>
		public List<string> Keys { get; set;}
		
		/// <summary>
		/// If true, return the list of all members of the shared group.
		/// </summary>
		public bool? GetMembers { get; set;}
		
		
	}
	
	
	
	public class GetSharedGroupDataResult
	{
		
		
		/// <summary>
		/// Data for the requested keys.
		/// </summary>
		public Dictionary<string,SharedGroupDataRecord> Data { get; set;}
		
		/// <summary>
		/// List of PlayFabId identifiers for the members of this group, if requested.
		/// </summary>
		public List<string> Members { get; set;}
		
		
	}
	
	
	
	public class GetTitleDataRequest
	{
		
		
		/// <summary>
		///  array of keys to get back data from the TitleData data blob, set by the admin tools
		/// </summary>
		public List<string> Keys { get; set;}
		
		
	}
	
	
	
	public class GetTitleDataResult
	{
		
		
		/// <summary>
		/// a dictionary object of key / value pairs
		/// </summary>
		public Dictionary<string,string> Data { get; set;}
		
		
	}
	
	
	
	public class GetUserAccountInfoRequest
	{
		
		
		public string PlayFabId { get; set;}
		
		
	}
	
	
	
	public class GetUserAccountInfoResult
	{
		
		
		/// <summary>
		/// Account info for the user whose information was requested.
		/// </summary>
		public UserAccountInfo UserInfo { get; set;}
		
		
	}
	
	
	
	public class GetUserDataRequest
	{
		
		
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// Specific keys to search for in the custom user data.
		/// </summary>
		public List<string> Keys { get; set;}
		
		/// <summary>
		/// The version that currently exists according to the caller. The call will return the data for all of the keys if the version in the system is greater than this.
		/// </summary>
		public int? IfChangedFromDataVersion { get; set;}
		
		
	}
	
	
	
	public class GetUserDataResult
	{
		
		
		/// <summary>
		/// PlayFab unique identifier of the user whose custom data is being returned.
		/// </summary>
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// Indicates the current version of the data that has been set. This is incremented with every set call for that type of data (read-only, internal, etc). This version can be provided in Get calls to find updated data.
		/// </summary>
		public uint DataVersion { get; set;}
		
		/// <summary>
		/// User specific data for this title.
		/// </summary>
		public Dictionary<string,UserDataRecord> Data { get; set;}
		
		
	}
	
	
	
	public class GetUserInventoryRequest
	{
		
		
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// Used to limit results to only those from a specific catalog version.
		/// </summary>
		public string CatalogVersion { get; set;}
		
		
	}
	
	
	
	public class GetUserInventoryResult
	{
		
		
		/// <summary>
		/// Array of inventory items belonging to the user.
		/// </summary>
		[Unordered(SortProperty="ItemInstanceId")]
		public List<ItemInstance> Inventory { get; set;}
		
		/// <summary>
		/// Array of virtual currency balance(s) belonging to the user.
		/// </summary>
		public Dictionary<string,int> VirtualCurrency { get; set;}
		
		/// <summary>
		/// Array of remaining times and timestamps for virtual currencies.
		/// </summary>
		public Dictionary<string,VirtualCurrencyRechargeTime> VirtualCurrencyRechargeTimes { get; set;}
		
		
	}
	
	
	
	public class GetUserStatisticsRequest
	{
		
		
		/// <summary>
		/// User for whom statistics are being requested.
		/// </summary>
		public string PlayFabId { get; set;}
		
		
	}
	
	
	
	public class GetUserStatisticsResult
	{
		
		
		/// <summary>
		/// User statistics for the requested user.
		/// </summary>
		public Dictionary<string,int> UserStatistics { get; set;}
		
		
	}
	
	
	
	public class GrantCharacterToUserRequest
	{
		
		
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// Non-unique display name of the character being granted.
		/// </summary>
		public string CharacterName { get; set;}
		
		/// <summary>
		/// Type of the character being granted; statistics can be sliced based on this value.
		/// </summary>
		public string CharacterType { get; set;}
		
		
	}
	
	
	
	public class GrantCharacterToUserResult
	{
		
		
		/// <summary>
		/// Unique identifier tagged to this character.
		/// </summary>
		public string CharacterId { get; set;}
		
		
	}
	
	
	
	public class GrantItemsToCharacterRequest
	{
		
		
		/// <summary>
		/// Catalog version from which items are to be granted.
		/// </summary>
		public string CatalogVersion { get; set;}
		
		public string CharacterId { get; set;}
		
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// String detailing any additional information concerning this operation.
		/// </summary>
		public string Annotation { get; set;}
		
		/// <summary>
		/// Array of itemIds to grant to the user.
		/// </summary>
		public List<string> ItemIds { get; set;}
		
		
	}
	
	
	
	public class GrantItemsToCharacterResult
	{
		
		
		/// <summary>
		/// Array of items granted to users.
		/// </summary>
		public List<ItemGrantResult> ItemGrantResults { get; set;}
		
		
	}
	
	
	
	public class GrantItemsToUserRequest
	{
		
		
		/// <summary>
		/// Catalog version from which items are to be granted.
		/// </summary>
		public string CatalogVersion { get; set;}
		
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// String detailing any additional information concerning this operation.
		/// </summary>
		public string Annotation { get; set;}
		
		/// <summary>
		/// Array of itemIds to grant to the user.
		/// </summary>
		public List<string> ItemIds { get; set;}
		
		
	}
	
	
	
	public class GrantItemsToUserResult
	{
		
		
		/// <summary>
		/// Array of items granted to users.
		/// </summary>
		public List<ItemGrantResult> ItemGrantResults { get; set;}
		
		
	}
	
	
	
	public class GrantItemsToUsersRequest
	{
		
		
		/// <summary>
		/// Catalog version from which items are to be granted.
		/// </summary>
		public string CatalogVersion { get; set;}
		
		/// <summary>
		/// Array of items to grant and the users to whom the items are to be granted.
		/// </summary>
		[Unordered]
		public List<ItemGrant> ItemGrants { get; set;}
		
		
	}
	
	
	
	public class GrantItemsToUsersResult
	{
		
		
		/// <summary>
		/// Array of items granted to users.
		/// </summary>
		public List<ItemGrantResult> ItemGrantResults { get; set;}
		
		
	}
	
	
	
	public class ItemGrant
	{
		
		
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// Unique identifier of the catalog item to be granted to the user.
		/// </summary>
		public string ItemId { get; set;}
		
		/// <summary>
		/// String detailing any additional information concerning this operation.
		/// </summary>
		public string Annotation { get; set;}
		
		public string CharacterId { get; set;}
		
		
	}
	
	
	
	/// <summary>
	/// Result of granting an item to a user
	/// </summary>
	public class ItemGrantResult : IComparable<ItemGrantResult>
	{
		
		
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// Unique identifier of the catalog item to be granted to the user.
		/// </summary>
		public string ItemId { get; set;}
		
		/// <summary>
		/// Unique instance Id of the granted item.
		/// </summary>
		public string ItemInstanceId { get; set;}
		
		/// <summary>
		/// String detailing any additional information concerning this operation.
		/// </summary>
		public string Annotation { get; set;}
		
		/// <summary>
		/// Result of this operation.
		/// </summary>
		public bool Result { get; set;}
		
		public string CharacterId { get; set;}
		
		
		public int CompareTo(ItemGrantResult other)
        {
            if (other == null || other.ItemInstanceId == null) return 1;
            if (ItemInstanceId == null) return -1;
            return ItemInstanceId.CompareTo(other.ItemInstanceId);
        }
		
	}
	
	
	
	/// <summary>
	/// A unique instance of an item in a user's inventory
	/// </summary>
	public class ItemInstance : IComparable<ItemInstance>
	{
		
		
		/// <summary>
		/// Unique identifier for the inventory item, as defined in the catalog.
		/// </summary>
		public string ItemId { get; set;}
		
		/// <summary>
		/// Unique item identifier for this specific instance of the item.
		/// </summary>
		public string ItemInstanceId { get; set;}
		
		/// <summary>
		/// Class name for the inventory item, as defined in the catalog.
		/// </summary>
		public string ItemClass { get; set;}
		
		/// <summary>
		/// Timestamp for when this instance was purchased.
		/// </summary>
		public DateTime? PurchaseDate { get; set;}
		
		/// <summary>
		/// Timestamp for when this instance will expire.
		/// </summary>
		public DateTime? Expiration { get; set;}
		
		/// <summary>
		/// Total number of remaining uses, if this is a consumable item.
		/// </summary>
		public int? RemainingUses { get; set;}
		
		/// <summary>
		/// Game specific comment associated with this instance when it was added to the user inventory.
		/// </summary>
		public string Annotation { get; set;}
		
		/// <summary>
		/// Catalog version for the inventory item, when this instance was created.
		/// </summary>
		public string CatalogVersion { get; set;}
		
		/// <summary>
		/// Unique identifier for the parent inventory item, as defined in the catalog, for object which were added from a bundle or container.
		/// </summary>
		public string BundleParent { get; set;}
		
		/// <summary>
		/// A set of custom key-value pairs on the inventory item.
		/// </summary>
		public Dictionary<string,string> CustomData { get; set;}
		
		
		public int CompareTo(ItemInstance other)
        {
            if (other == null || other.ItemInstanceId == null) return 1;
            if (ItemInstanceId == null) return -1;
            return ItemInstanceId.CompareTo(other.ItemInstanceId);
        }
		
	}
	
	
	
	public class ListUsersCharactersRequest
	{
		
		
		public string PlayFabId { get; set;}
		
		
	}
	
	
	
	public class ListUsersCharactersResult
	{
		
		
		public List<CharacterResult> Characters { get; set;}
		
		
	}
	
	
	
	public class LogEventRequest
	{
		
		
		/// <summary>
		/// PlayFab User Id of the player associated with this event. For non-player associated events, this must be null and EntityId must be set.
		/// </summary>
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// For non player-associated events, a unique ID for the entity associated with this event. For player associated events, this must be null and PlayFabId must be set.
		/// </summary>
		public string EntityId { get; set;}
		
		/// <summary>
		/// For non player-associated events, the type of entity associated with this event. For player associated events, this must be null.
		/// </summary>
		public string EntityType { get; set;}
		
		/// <summary>
		/// Optional timestamp for this event. If null, the a timestamp is auto-assigned to the event on the server.
		/// </summary>
		public DateTime? Timestamp { get; set;}
		
		/// <summary>
		/// A unique event name which will be used as the table name in the Redshift database. The name will be made lower case, and cannot not contain spaces. The use of underscores is recommended, for readability. Events also cannot match reserved terms. The PlayFab reserved terms are 'log_in' and 'purchase', 'create' and 'request', while the Redshift reserved terms can be found here: http://docs.aws.amazon.com/redshift/latest/dg/r_pg_keywords.html.
		/// </summary>
		public string EventName { get; set;}
		
		/// <summary>
		/// Contains all the data for this event. Event Values can be strings, booleans or numerics (float, double, integer, long) and must be consistent on a per-event basis (if the Value for Key 'A' in Event 'Foo' is an integer the first time it is sent, it must be an integer in all subsequent 'Foo' events). As with event names, Keys must also not use reserved words (see above). Finally, the size of the Body for an event must be less than 32KB (UTF-8 format).
		/// </summary>
		public Dictionary<string,object> Body { get; set;}
		
		/// <summary>
		/// Flag to set event Body as profile details in the Redshift database as well as a standard event.
		/// </summary>
		public bool ProfileSetEvent { get; set;}
		
		
	}
	
	
	
	public class LogEventResult
	{
		
		
		
	}
	
	
	
	public class ModifyCharacterVirtualCurrencyResult
	{
		
		
		/// <summary>
		/// Name of the virtual currency which was modified.
		/// </summary>
		public string VirtualCurrency { get; set;}
		
		/// <summary>
		/// Balance of the virtual currency after modification.
		/// </summary>
		public int Balance { get; set;}
		
		
	}
	
	
	
	public class ModifyItemUsesRequest
	{
		
		
		/// <summary>
		/// PlayFab unique identifier of the user whose item is being modified.
		/// </summary>
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// Unique instance identifier of the item to be modified.
		/// </summary>
		public string ItemInstanceId { get; set;}
		
		/// <summary>
		/// Number of uses to add to the item. Can be negative to remove uses.
		/// </summary>
		public int UsesToAdd { get; set;}
		
		
	}
	
	
	
	public class ModifyItemUsesResult
	{
		
		
		/// <summary>
		/// Unique instance identifier of the item with uses consumed.
		/// </summary>
		public string ItemInstanceId { get; set;}
		
		/// <summary>
		/// Number of uses remaining on the item.
		/// </summary>
		public int RemainingUses { get; set;}
		
		
	}
	
	
	
	public class ModifyUserVirtualCurrencyResult
	{
		
		
		/// <summary>
		/// User currency was subtracted from.
		/// </summary>
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// Name of the virtual currency which was modified.
		/// </summary>
		public string VirtualCurrency { get; set;}
		
		/// <summary>
		/// Amount added or subtracted from the user's virtual currency.
		/// </summary>
		public int BalanceChange { get; set;}
		
		/// <summary>
		/// Balance of the virtual currency after modification.
		/// </summary>
		public int Balance { get; set;}
		
		
	}
	
	
	
	public class MoveItemToCharacterFromCharacterRequest
	{
		
		
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// Unique identifier of the character that currently has the item.
		/// </summary>
		public string GivingCharacterId { get; set;}
		
		/// <summary>
		/// Unique identifier of the character that will be receiving the item.
		/// </summary>
		public string ReceivingCharacterId { get; set;}
		
		public string ItemInstanceId { get; set;}
		
		
	}
	
	
	
	public class MoveItemToCharacterFromCharacterResult
	{
		
		
		
	}
	
	
	
	public class MoveItemToCharacterFromUserRequest
	{
		
		
		public string PlayFabId { get; set;}
		
		public string CharacterId { get; set;}
		
		public string ItemInstanceId { get; set;}
		
		
	}
	
	
	
	public class MoveItemToCharacterFromUserResult
	{
		
		
		
	}
	
	
	
	public class MoveItemToUserFromCharacterRequest
	{
		
		
		public string PlayFabId { get; set;}
		
		public string CharacterId { get; set;}
		
		public string ItemInstanceId { get; set;}
		
		
	}
	
	
	
	public class MoveItemToUserFromCharacterResult
	{
		
		
		
	}
	
	
	
	public class NotifyMatchmakerPlayerLeftRequest
	{
		
		
		/// <summary>
		/// Unique identifier of the Game Instance the user is leaving.
		/// </summary>
		public string LobbyId { get; set;}
		
		public string PlayFabId { get; set;}
		
		
	}
	
	
	
	public class NotifyMatchmakerPlayerLeftResult
	{
		
		
		/// <summary>
		/// State of user leaving the Game Server Instance.
		/// </summary>
		[JsonConverter(typeof(StringEnumConverter))]
		public PlayerConnectionState? PlayerState { get; set;}
		
		
	}
	
	
	
	public enum PlayerConnectionState
	{
		Unassigned,
		Connecting,
		Participating,
		Participated,
		Reconnecting
	}
	
	
	
	public class PlayerLeaderboardEntry
	{
		
		
		/// <summary>
		/// PlayFab unique identifier of the user for this leaderboard entry.
		/// </summary>
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// Title-specific display name of the user for this leaderboard entry.
		/// </summary>
		public string DisplayName { get; set;}
		
		/// <summary>
		/// Specific value of the user's statistic.
		/// </summary>
		public int StatValue { get; set;}
		
		/// <summary>
		/// User's overall position in the leaderboard.
		/// </summary>
		public int Position { get; set;}
		
		
	}
	
	
	
	public class RedeemMatchmakerTicketRequest
	{
		
		
		/// <summary>
		/// Server authorization ticket passed back from a call to Matchmake or StartGame.
		/// </summary>
		public string Ticket { get; set;}
		
		/// <summary>
		/// Unique identifier of the Game Server Instance that is asking for validation of the authorization ticket.
		/// </summary>
		public string LobbyId { get; set;}
		
		
	}
	
	
	
	public class RedeemMatchmakerTicketResult
	{
		
		
		/// <summary>
		/// Boolean indicating whether the ticket was validated by the PlayFab service.
		/// </summary>
		public bool TicketIsValid { get; set;}
		
		/// <summary>
		/// Error value if the ticket was not validated.
		/// </summary>
		public string Error { get; set;}
		
		/// <summary>
		/// User account information for the user validated.
		/// </summary>
		public UserAccountInfo UserInfo { get; set;}
		
		
	}
	
	
	
	public class RemoveSharedGroupMembersRequest
	{
		
		
		/// <summary>
		/// Unique identifier for the shared group.
		/// </summary>
		public string SharedGroupId { get; set;}
		
		public List<string> PlayFabIds { get; set;}
		
		
	}
	
	
	
	public class RemoveSharedGroupMembersResult
	{
		
		
		
	}
	
	
	
	public class ReportPlayerServerRequest
	{
		
		
		/// <summary>
		/// PlayFabId of the reporting player.
		/// </summary>
		public string ReporterId { get; set;}
		
		/// <summary>
		/// PlayFabId of the reported player.
		/// </summary>
		public string ReporteeId { get; set;}
		
		/// <summary>
		/// Title player was reported in, optional if report not for specific title.
		/// </summary>
		public string TitleId { get; set;}
		
		/// <summary>
		/// Optional additional comment by reporting player.
		/// </summary>
		public string Comment { get; set;}
		
		
	}
	
	
	
	public class ReportPlayerServerResult
	{
		
		
		public bool Updated { get; set;}
		
		public int SubmissionsRemaining { get; set;}
		
		
	}
	
	
	
	public class SendPushNotificationRequest
	{
		
		
		/// <summary>
		/// PlayFabId of the recipient of the push notification.
		/// </summary>
		public string Recipient { get; set;}
		
		/// <summary>
		/// Text of message to send.
		/// </summary>
		public string Message { get; set;}
		
		/// <summary>
		/// Subject of message to send (may not be displayed in all platforms.
		/// </summary>
		public string Subject { get; set;}
		
		
	}
	
	
	
	public class SendPushNotificationResult
	{
		
		
		
	}
	
	
	
	public class SetPublisherDataRequest
	{
		
		
		/// <summary>
		/// key we want to set a value on (note, this is additive - will only replace an existing key's value if they are the same name.) Keys are trimmed of whitespace. Keys may not begin with the '!' character.
		/// </summary>
		public string Key { get; set;}
		
		/// <summary>
		/// new value to set. Set to null to remove a value
		/// </summary>
		public string Value { get; set;}
		
		
	}
	
	
	
	public class SetPublisherDataResult
	{
		
		
		
	}
	
	
	
	public class SetTitleDataRequest
	{
		
		
		/// <summary>
		/// key we want to set a value on (note, this is additive - will only replace an existing key's value if they are the same name.) Keys are trimmed of whitespace. Keys may not begin with the '!' character.
		/// </summary>
		public string Key { get; set;}
		
		/// <summary>
		/// new value to set. Set to null to remove a value
		/// </summary>
		public string Value { get; set;}
		
		
	}
	
	
	
	public class SetTitleDataResult
	{
		
		
		
	}
	
	
	
	public class SharedGroupDataRecord
	{
		
		
		/// <summary>
		/// Data stored for the specified group data key.
		/// </summary>
		public string Value { get; set;}
		
		/// <summary>
		/// PlayFabId of the user to last update this value.
		/// </summary>
		public string LastUpdatedBy { get; set;}
		
		/// <summary>
		/// Timestamp for when this data was last updated.
		/// </summary>
		public DateTime LastUpdated { get; set;}
		
		/// <summary>
		/// Indicates whether this data can be read by all users (public) or only members of the group (private).
		/// </summary>
		[JsonConverter(typeof(StringEnumConverter))]
		public UserDataPermission? Permission { get; set;}
		
		
	}
	
	
	
	public class SubtractCharacterVirtualCurrencyRequest
	{
		
		
		public string PlayFabId { get; set;}
		
		public string CharacterId { get; set;}
		
		/// <summary>
		/// Name of the virtual currency which is to be decremented.
		/// </summary>
		public string VirtualCurrency { get; set;}
		
		/// <summary>
		/// Amount to be subtracted from the user balance of the specified virtual currency.
		/// </summary>
		public int Amount { get; set;}
		
		
	}
	
	
	
	public class SubtractUserVirtualCurrencyRequest
	{
		
		
		/// <summary>
		/// PlayFab unique identifier of the user whose virtual currency balance is to be decreased.
		/// </summary>
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// Name of the virtual currency which is to be decremented.
		/// </summary>
		public string VirtualCurrency { get; set;}
		
		/// <summary>
		/// Amount to be subtracted from the user balance of the specified virtual currency.
		/// </summary>
		public int Amount { get; set;}
		
		
	}
	
	
	
	public enum TitleActivationStatus
	{
		None,
		ActivatedTitleKey,
		PendingSteam,
		ActivatedSteam,
		RevokedSteam
	}
	
	
	
	public class UpdateCharacterDataRequest
	{
		
		
		public string PlayFabId { get; set;}
		
		public string CharacterId { get; set;}
		
		/// <summary>
		/// Data to be written to the user's character's custom data. Note that keys are trimmed of whitespace, are limited to 1024 characters, and may not begin with a '!' character.
		/// </summary>
		public Dictionary<string,string> Data { get; set;}
		
		/// <summary>
		/// Permission to be applied to all user data keys written in this request. Defaults to "private" if not set.
		/// </summary>
		[JsonConverter(typeof(StringEnumConverter))]
		public UserDataPermission? Permission { get; set;}
		
		
	}
	
	
	
	public class UpdateCharacterDataResult
	{
		
		
		/// <summary>
		/// Indicates the current version of the data that has been set. This is incremented with every set call for that type of data (read-only, internal, etc). This version can be provided in Get calls to find updated data.
		/// </summary>
		public uint DataVersion { get; set;}
		
		
	}
	
	
	
	public class UpdateCharacterStatisticsRequest
	{
		
		
		public string PlayFabId { get; set;}
		
		public string CharacterId { get; set;}
		
		/// <summary>
		/// Statistics to be updated with the provided values.
		/// </summary>
		public Dictionary<string,int> CharacterStatistics { get; set;}
		
		
	}
	
	
	
	public class UpdateCharacterStatisticsResult
	{
		
		
		
	}
	
	
	
	public class UpdateSharedGroupDataRequest
	{
		
		
		/// <summary>
		/// Unique identifier for the shared group.
		/// </summary>
		public string SharedGroupId { get; set;}
		
		/// <summary>
		/// Key value pairs to be stored in the shared group - note that keys will be trimmed of whitespace, must not begin with a '!' character, and that null values will result in the removal of the key from the data set.
		/// </summary>
		public Dictionary<string,string> Data { get; set;}
		
		/// <summary>
		/// Permission to be applied to all user data keys in this request.
		/// </summary>
		[JsonConverter(typeof(StringEnumConverter))]
		public UserDataPermission? Permission { get; set;}
		
		
	}
	
	
	
	public class UpdateSharedGroupDataResult
	{
		
		
		
	}
	
	
	
	public class UpdateUserDataRequest
	{
		
		
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// Data to be written to the user's custom data. Note that keys are trimmed of whitespace, are limited to 1024 characters, and may not begin with a '!' character.
		/// </summary>
		public Dictionary<string,string> Data { get; set;}
		
		/// <summary>
		/// Permission to be applied to all user data keys written in this request. Defaults to "private" if not set.
		/// </summary>
		[JsonConverter(typeof(StringEnumConverter))]
		public UserDataPermission? Permission { get; set;}
		
		
	}
	
	
	
	public class UpdateUserDataResult
	{
		
		
		/// <summary>
		/// Indicates the current version of the data that has been set. This is incremented with every set call for that type of data (read-only, internal, etc). This version can be provided in Get calls to find updated data.
		/// </summary>
		public uint DataVersion { get; set;}
		
		
	}
	
	
	
	public class UpdateUserInternalDataRequest
	{
		
		
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// Data to be written to the user's custom data.
		/// </summary>
		public Dictionary<string,string> Data { get; set;}
		
		
	}
	
	
	
	public class UpdateUserInventoryItemDataRequest
	{
		
		
		public string CharacterId { get; set;}
		
		public string PlayFabId { get; set;}
		
		public string ItemInstanceId { get; set;}
		
		/// <summary>
		/// Data to be written to the item's custom data. Note that keys are trimmed of whitespace.
		/// </summary>
		public Dictionary<string,string> Data { get; set;}
		
		
	}
	
	
	
	public class UpdateUserInventoryItemDataResult
	{
		
		
		
	}
	
	
	
	public class UpdateUserStatisticsRequest
	{
		
		
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// Statistics to be updated with the provided values.
		/// </summary>
		public Dictionary<string,int> UserStatistics { get; set;}
		
		
	}
	
	
	
	public class UpdateUserStatisticsResult
	{
		
		
		
	}
	
	
	
	public class UserAccountInfo
	{
		
		
		/// <summary>
		/// unique identifier for the user account
		/// </summary>
		public string PlayFabId { get; set;}
		
		/// <summary>
		/// timestamp indicating when the user account was created
		/// </summary>
		public DateTime Created { get; set;}
		
		/// <summary>
		/// user account name in the PlayFab service
		/// </summary>
		public string Username { get; set;}
		
		/// <summary>
		/// title-specific information for the user account
		/// </summary>
		public UserTitleInfo TitleInfo { get; set;}
		
		/// <summary>
		/// personal information for the user which is considered more sensitive
		/// </summary>
		public UserPrivateAccountInfo PrivateInfo { get; set;}
		
		/// <summary>
		/// user Facebook information, if a Facebook account has been linked
		/// </summary>
		public UserFacebookInfo FacebookInfo { get; set;}
		
		/// <summary>
		/// user Steam information, if a Steam account has been linked
		/// </summary>
		public UserSteamInfo SteamInfo { get; set;}
		
		/// <summary>
		/// user Gamecenter information, if a Gamecenter account has been linked
		/// </summary>
		public UserGameCenterInfo GameCenterInfo { get; set;}
		
		
	}
	
	
	
	public enum UserDataPermission
	{
		Private,
		Public
	}
	
	
	
	public class UserDataRecord
	{
		
		
		/// <summary>
		/// User-supplied data for this user data key.
		/// </summary>
		public string Value { get; set;}
		
		/// <summary>
		/// Timestamp indicating when this data was last updated.
		/// </summary>
		public DateTime LastUpdated { get; set;}
		
		/// <summary>
		/// Permissions on this data key.
		/// </summary>
		[JsonConverter(typeof(StringEnumConverter))]
		public UserDataPermission? Permission { get; set;}
		
		
	}
	
	
	
	public class UserFacebookInfo
	{
		
		
		/// <summary>
		/// Facebook identifier
		/// </summary>
		public string FacebookId { get; set;}
		
		/// <summary>
		/// Facebook full name
		/// </summary>
		public string FullName { get; set;}
		
		
	}
	
	
	
	public class UserGameCenterInfo
	{
		
		
		/// <summary>
		/// Gamecenter identifier
		/// </summary>
		public string GameCenterId { get; set;}
		
		
	}
	
	
	
	public enum UserOrigination
	{
		Organic,
		Steam,
		Google,
		Amazon,
		Facebook,
		Kongregate,
		GamersFirst,
		Unknown,
		IOS,
		LoadTest,
		Android,
		PSN,
		GameCenter
	}
	
	
	
	public class UserPrivateAccountInfo
	{
		
		
		/// <summary>
		/// user email address
		/// </summary>
		public string Email { get; set;}
		
		
	}
	
	
	
	public class UserSteamInfo
	{
		
		
		/// <summary>
		/// Steam identifier
		/// </summary>
		public string SteamId { get; set;}
		
		/// <summary>
		/// the country in which the player resides, from Steam data
		/// </summary>
		public string SteamCountry { get; set;}
		
		/// <summary>
		/// currency type set in the user Steam account
		/// </summary>
		[JsonConverter(typeof(StringEnumConverter))]
		public Currency? SteamCurrency { get; set;}
		
		/// <summary>
		/// what stage of game ownership the user is listed as being in, from Steam
		/// </summary>
		[JsonConverter(typeof(StringEnumConverter))]
		public TitleActivationStatus? SteamActivationStatus { get; set;}
		
		
	}
	
	
	
	public class UserTitleInfo
	{
		
		
		/// <summary>
		/// name of the user, as it is displayed in-game
		/// </summary>
		public string DisplayName { get; set;}
		
		/// <summary>
		/// source by which the user first joined the game, if known
		/// </summary>
		[JsonConverter(typeof(StringEnumConverter))]
		public UserOrigination? Origination { get; set;}
		
		/// <summary>
		/// timestamp indicating when the user was first associated with this game (this can differ significantly from when the user first registered with PlayFab)
		/// </summary>
		public DateTime Created { get; set;}
		
		/// <summary>
		/// timestamp for the last user login for this title
		/// </summary>
		public DateTime? LastLogin { get; set;}
		
		/// <summary>
		/// timestamp indicating when the user first signed into this game (this can differ from the Created timestamp, as other events, such as issuing a beta key to the user, can associate the title to the user)
		/// </summary>
		public DateTime? FirstLogin { get; set;}
		
		/// <summary>
		/// boolean indicating whether or not the user is currently banned for a title
		/// </summary>
		public bool? isBanned { get; set;}
		
		
	}
	
	
	
	public class VirtualCurrencyRechargeTime
	{
		
		
		/// <summary>
		/// Time remaining (in seconds) before the next recharge increment of the virtual currency.
		/// </summary>
		public int SecondsToRecharge { get; set;}
		
		/// <summary>
		/// Server timestamp in UTC indicating the next time the virtual currency will be incremented.
		/// </summary>
		public DateTime RechargeTime { get; set;}
		
		/// <summary>
		/// Maximum value to which the regenerating currency will automatically increment. Note that it can exceed this value through use of the AddUserVirtualCurrency API call. However, it will not regenerate automatically until it has fallen below this value.
		/// </summary>
		public int RechargeMax { get; set;}
		
		
	}
	
}
