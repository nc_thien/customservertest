﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Lidgren.Network;

using Microsoft.Test.CommandLineParsing;

using PlayFab;
using PlayFab.ServerModels;

namespace TestServer
{
    class Player
    {
        public long id;
    }

    class Program
    {
        private static string lobbyId;

        private static StreamWriter logWriter;

        private static async void RedeemMatchmaker(string ticket, NetConnection connection)
        {
            if (string.IsNullOrEmpty(lobbyId))
            {
                //await Task.Delay(20000);

                connection.Approve();
                return;
            }

            RedeemMatchmakerTicketRequest request = new RedeemMatchmakerTicketRequest();
            request.LobbyId = lobbyId;
            request.Ticket = ticket;

            PlayFabResult<RedeemMatchmakerTicketResult> result = await PlayFabServerAPI.RedeemMatchmakerTicketAsync(request);

            if (result.Error == null)
            {
                if (result.Result.TicketIsValid)
                {
                    Log("RedeemMatchmaker " + ticket + " valid " + result.Result.UserInfo.PlayFabId);
                    connection.Approve();
                }
                else
                {
                    Log("RedeemMatchmaker " + ticket + " invalid");
                    connection.Deny();
                }
            }
            else
            {
                Log("ERROR : " + "RedeemMatchmaker " + ticket + " " + result.Error.ErrorMessage);

                if (result.Error.Error == PlayFabErrorCode.ExpiredGameTicket) connection.Approve(); //HACK TO BYPASS EXPIRED ERROR
                else connection.Deny();
            }
        }

        private static async void NotifyMatchmakerPlayerLeft(string playFabId)
        {
            if (string.IsNullOrEmpty(lobbyId)) return;

            NotifyMatchmakerPlayerLeftRequest request = new NotifyMatchmakerPlayerLeftRequest();
            request.LobbyId = lobbyId;
            request.PlayFabId = playFabId;

            PlayFabResult<NotifyMatchmakerPlayerLeftResult> result = await PlayFabServerAPI.NotifyMatchmakerPlayerLeftAsync(request);

            if (result.Error == null)
            {
                Log("NotifyMatchmakerPlayerLeft " + playFabId);
            }
            else
            {
                Log("ERROR : " + "NotifyMatchmakerPlayerLeft" + " " + result.Error.ErrorMessage);
            }
        }

        private static void Log(string message)
        {
            if (logWriter != null)
            {
                logWriter.WriteLine(message);
                logWriter.Flush();
            }
        }

        static void Main(string[] args)
        {
            CommandLineDictionary d = CommandLineDictionary.FromArguments(args, '-', '=');

            string game_id = d["game_id"];
            string game_build_version = d["game_build_version"];
            string game_mode = d["game_mode"];
            string server_host_domain = d["server_host_domain"];
            string server_host_port = d["server_host_port"];
            string server_host_region = d["server_host_region"];
            string playfab_api_endpoint = d["playfab_api_endpoint"];
            string title_secret_key = d["title_secret_key"];
            string custom_data = d["custom_data"];
            string log_file_path = d["log_file_path"];
            string output_files_directory_path = d["output_files_directory_path"];

            lobbyId = game_id;

            PlayFabSettings.ProductionEnvironmentURL = playfab_api_endpoint;
            PlayFabSettings.DeveloperSecretKey = title_secret_key;

            logWriter = new StreamWriter(output_files_directory_path + "/" + "log.txt");

            Log(game_id);
            Log(game_build_version);
            Log(game_mode);
            Log(server_host_domain);
            Log(server_host_port);
            Log(server_host_region);
            Log(playfab_api_endpoint);
            Log(title_secret_key);
            Log(custom_data);
            Log(log_file_path);
            Log(output_files_directory_path);

            NetPeerConfiguration config = new NetPeerConfiguration("test_network");
            config.EnableMessageType(NetIncomingMessageType.DiscoveryRequest);
            config.Port = int.Parse(server_host_port);

            config.EnableMessageType(NetIncomingMessageType.WarningMessage);
            config.EnableMessageType(NetIncomingMessageType.VerboseDebugMessage);
            config.EnableMessageType(NetIncomingMessageType.ErrorMessage);
            config.EnableMessageType(NetIncomingMessageType.Error);
            config.EnableMessageType(NetIncomingMessageType.DebugMessage);
            config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);

            NetServer server = new NetServer(config);
            server.Start();

            List<Player> players = new List<Player>();

            // schedule initial sending of position updates
            double nextSendUpdates = NetTime.Now;

            double lastPlayTime = NetTime.Now;

            CreatePlayerMessage createPlayerMessage = new CreatePlayerMessage();
            RemovePlayerMessage removePlayerMessage = new RemovePlayerMessage();

            UpdatePlayerInputMessage updatePlayerInputMessage = new UpdatePlayerInputMessage();
            PressFireMessage pressFireMessage = new PressFireMessage();

            EntityManager entityManager = new EntityManager(server, true);

            // run until escape is pressed
            //while (!Console.KeyAvailable || Console.ReadKey().Key != ConsoleKey.Escape)

            const float UPDATE_TIME = 1.0f / 30.0f;

            while (NetTime.Now - lastPlayTime < 30.0f) //Wait for 30s before disconnecting
            {
                NetIncomingMessage msg;
                while ((msg = server.ReadMessage()) != null)
                {
                    switch (msg.MessageType)
                    {
                        case NetIncomingMessageType.VerboseDebugMessage:
                        case NetIncomingMessageType.DebugMessage:
                        case NetIncomingMessageType.WarningMessage:
                        case NetIncomingMessageType.ErrorMessage:
                            //
                            // Just print diagnostic messages to console
                            //
                            Console.WriteLine(msg.ReadString());
                            break;
                            
                        case NetIncomingMessageType.ConnectionApproval:
                            {
                                string playFabId = msg.ReadString();
                                string ticket = msg.ReadString();

                                msg.SenderConnection.Tag = playFabId;
                                RedeemMatchmaker(ticket, msg.SenderConnection);
                            }
                            break;

                        case NetIncomingMessageType.StatusChanged:
                            NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();
                            if (status == NetConnectionStatus.Connected)
                            {
                                //
                                // A new player just connected!
                                //

                                long id = msg.SenderConnection.RemoteUniqueIdentifier;
                                Console.WriteLine(NetUtility.ToHexString(id) + " connected!");
                                
                                int pos = -1;

                                Player player = new Player();
                                player.id = id;

                                int numPlayers = players.Count;
                                for (int i = 0; i < numPlayers; ++i)
                                    if (players[i] == null)
                                    {
                                        pos = i;
                                        players[i] = player;
                                        break;
                                    }

                                if (pos == -1)
                                {
                                    pos = numPlayers++;
                                    players.Add(player);
                                }

                                entityManager.InitWorldState(msg.SenderConnection);

                                Ship ship = entityManager.CreateShip();
                                entityManager.SetShip(ship, pos);

                                createPlayerMessage.id = pos;
                                createPlayerMessage.isLocal = false;
                                createPlayerMessage.x = ship.x;
                                createPlayerMessage.y = ship.y;
                                createPlayerMessage.SendAll(server, msg.SenderConnection, NetDeliveryMethod.ReliableOrdered);

                                createPlayerMessage.isLocal = true;
                                createPlayerMessage.Send(server, msg.SenderConnection, NetDeliveryMethod.ReliableOrdered);
                            }
                            else if (status == NetConnectionStatus.Disconnected)
                            {
                                object tag = msg.SenderConnection.Tag;
                                if (tag != null)
                                {
                                    string playFabId = (string)tag;
                                    NotifyMatchmakerPlayerLeft(playFabId);
                                }

                                long id = msg.SenderConnection.RemoteUniqueIdentifier;
                                Console.WriteLine(NetUtility.ToHexString(id) + " disconnected!");

                                int pos = -1;

                                int numPlayers = players.Count;
                                for (int i = 0; i < numPlayers; ++i)
                                    if (players[i] != null && players[i].id == id)
                                    {
                                        pos = i;
                                        break;
                                    }

                                if (pos >= 0)
                                {
                                    entityManager.RemoveShip(pos);

                                    players[pos] = null;

                                    removePlayerMessage.id = pos;
                                    removePlayerMessage.SendAll(server, NetDeliveryMethod.ReliableOrdered);
                                }
                            }
                            break;

                        case NetIncomingMessageType.Data:
                            var gameMessageType = (GameMessageTypes)msg.ReadByte();

                            switch (gameMessageType)
                            {
                                case GameMessageTypes.UpdatePlayerInput:
                                    {
                                        updatePlayerInputMessage.Decode(msg);
                                        int id = updatePlayerInputMessage.id;

                                        entityManager.UpdateShipInput(id, updatePlayerInputMessage.h, updatePlayerInputMessage.v);
                                    }
                                    break;

                                case GameMessageTypes.PressFire:
                                    {
                                        pressFireMessage.Decode(msg);
                                        int id = pressFireMessage.id;

                                        entityManager.PressFire(id, pressFireMessage.isPressed);
                                    }
                                    break;

                                default:
                                    break;
                            }

                            break;
                    }
                }

                double now = NetTime.Now;

                if (players.Count > 0) lastPlayTime = now;

                //
                // send updates
                //
                if (now > nextSendUpdates)
                {
                    entityManager.Update(UPDATE_TIME);

                    // schedule next update
                    nextSendUpdates += UPDATE_TIME;
                }

                // sleep to allow other processes to run smoothly
                Thread.Sleep(1);
            }

            if (logWriter != null)
            {
                logWriter.Close();
                logWriter.Dispose();

                logWriter = null;
            }

            server.Shutdown("app exiting");
        }
    }
}
