using UnityEngine;
using System.Collections;

public class DestroyAfterTime : MonoBehaviour
{
	public float time;

	// Use this for initialization
	void Start ()
	{
		GameObject.Destroy(gameObject, time);	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}

