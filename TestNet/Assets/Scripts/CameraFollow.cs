using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
	private Transform cachedTransform;
	public Transform followObject = null;
	//private Vector3 vel = Vector3.zero;

	void Awake()
	{
		cachedTransform = transform;
	}

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (followObject != null)
		{
			Vector3 targetPos = followObject.position;
			targetPos.z -= 10.0f;

			//cachedTransform.position = Vector3.SmoothDamp(cachedTransform.position, targetPos, ref vel, 0.1f);
			cachedTransform.position = targetPos;
		}
	}
}

