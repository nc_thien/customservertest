﻿#define TEST_MODE

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;

using PlayFab;
using PlayFab.ClientModels;

using Lidgren.Network;

public class Game : MonoBehaviour 
{
	public static Game Instance = null;

	public static int GameState = 2;

	public string PlayFabTitleId = string.Empty;
	public string PlayFabId = string.Empty;

	public GameObject shipPrefab;
	public GameObject bulletPrefab;

	public GameObject explosionPrefab;
	
	NetClient client;

	private CreatePlayerMessage createPlayerMessage;
	private RemovePlayerMessage removePlayerMessage;
	
	private UpdatePlayerInputMessage updatePlayerInputMessage;
	private PressFireMessage pressFireMessage;

	private UpdatePlayerMessage updatePlayerMessage;

	private DisableShipInvincibleMessage disableShipInvincibleMessage;
	private RespawnShipMessage respawnShipMessage;

	private GameMessageList<CreateBulletMessage> addBulletList;
	private GameMessageList<UpdateBulletMessage> updateBulletList;
	private GameMessageList<RemoveBulletMessage> removeBulletList;

	private EntityManager entityManager;

	private bool pressFire;

	void Awake() 
	{
		Instance = this;

		PlayFabSettings.TitleId = PlayFabTitleId;

		NetPeerConfiguration config = new NetPeerConfiguration("test_network");
		config.EnableMessageType(NetIncomingMessageType.WarningMessage);
		config.EnableMessageType(NetIncomingMessageType.VerboseDebugMessage);
		config.EnableMessageType(NetIncomingMessageType.ErrorMessage);
		config.EnableMessageType(NetIncomingMessageType.Error);
		config.EnableMessageType(NetIncomingMessageType.DebugMessage);
		config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);

		config.MaximumHandshakeAttempts = 10;

		client = new NetClient(config);

		createPlayerMessage = new CreatePlayerMessage();
		removePlayerMessage = new RemovePlayerMessage();
		
		updatePlayerInputMessage = new UpdatePlayerInputMessage();
		pressFireMessage = new PressFireMessage();

		updatePlayerMessage = new UpdatePlayerMessage();

		addBulletList = new GameMessageList<CreateBulletMessage>();
		updateBulletList = new GameMessageList<UpdateBulletMessage>();
		removeBulletList = new GameMessageList<RemoveBulletMessage>();

		disableShipInvincibleMessage = new DisableShipInvincibleMessage();
		respawnShipMessage = new RespawnShipMessage();
		
		entityManager = new EntityManager(client, false);
		entityManager.OnCreatBullet += CreateBullet;
		entityManager.OnRemoveBullet += RemoveBullet;
	}

	void Start()
	{
		pressFire = false;

	#if TEST_MODE
		StartGame();
	#endif	
	}

	public void AddPlayer(int id, bool isLocal, float x, float y)
	{
		Ship ship = entityManager.CreateShip(x, y);

		entityManager.SetShip(ship, id);
		
		GameObject gameObject = GameObject.Instantiate(shipPrefab) as GameObject;
		ShipDisplay shipDisplay = gameObject.GetComponent<ShipDisplay>();
		shipDisplay.SetShip(ship);

		ship.tag = gameObject;

		if (isLocal)
		{
 			entityManager.localId = id;
			Camera.main.GetComponent<CameraFollow>().followObject = gameObject.transform;
		}
	}

	public void RemovePlayer(int id)
	{
		Ship ship = entityManager.GetShip(id);

		if (ship != null)
		{
			entityManager.RemoveShip(id);
			GameObject gameObject = ship.tag as GameObject;
			GameObject.Destroy(gameObject);
		}
	}

	public void RespawnShip(int id, float x, float y)
	{
		Ship ship = entityManager.GetShip(id);
		
		if (ship != null)
		{
			ship.x = x;
			ship.y = y;

			ship.speed = 0.0f;

			GameObject gameObject = ship.tag as GameObject;
			GameObject.Instantiate(explosionPrefab, gameObject.transform.position, Quaternion.identity);

			ShipDisplay shipDisplay = gameObject.GetComponent<ShipDisplay>();
			shipDisplay.SetShip(ship);
		}		
	}
	
	public void CreateBullet(Bullet bullet)
	{
		GameObject gameObject = GameObject.Instantiate(bulletPrefab) as GameObject;
		BulletDisplay bulletDisplay = gameObject.GetComponent<BulletDisplay>();
		bulletDisplay.SetBullet(bullet);
		
		bullet.tag = gameObject;
	}

	public void RemoveBullet(Bullet bullet)
	{
		GameObject gameObject = bullet.tag as GameObject;
		GameObject.Destroy(gameObject);
	}

	private void UpdateInput()
	{
		if (entityManager.localId < 0) return;

		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");

		if (h != 0.0f || v != 0.0f)
		{
			float dt = Time.deltaTime;

			updatePlayerInputMessage.id = entityManager.localId;
			updatePlayerInputMessage.h = h * dt;
			updatePlayerInputMessage.v = v * dt;
			
			updatePlayerInputMessage.Send(client, NetDeliveryMethod.Unreliable);
		}

		bool pressFire = Input.GetButton("Fire1");
		
		if (pressFire != this.pressFire)
		{
			this.pressFire = pressFire;
			
			pressFireMessage.id = entityManager.localId;
			pressFireMessage.isPressed = pressFire;

			pressFireMessage.Send(client, NetDeliveryMethod.ReliableOrdered);
		}
	}
	
	void Update()
	{
		UpdateInput();

		entityManager.Update(Time.deltaTime);

		// read messages
		NetIncomingMessage msg;
		while ((msg = client.ReadMessage()) != null)
		{
			switch (msg.MessageType)
			{
			case NetIncomingMessageType.StatusChanged:
				NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();
				if (status == NetConnectionStatus.Connected)
				{
					//
					// A new player just connected!
					//
					Debug.Log(NetUtility.ToHexString(msg.SenderConnection.RemoteUniqueIdentifier) + " connected!");
				}
				else if (status == NetConnectionStatus.Disconnected)
				{
					Debug.Log(NetUtility.ToHexString(msg.SenderConnection.RemoteUniqueIdentifier) + " disconnected!");
				}
				
				break;

			case NetIncomingMessageType.Data:
				var gameMessageType = (GameMessageTypes)msg.ReadByte();

				switch (gameMessageType)
				{
				case GameMessageTypes.CreatePlayer:
				{
					createPlayerMessage.Decode(msg);
					AddPlayer(createPlayerMessage.id, createPlayerMessage.isLocal, createPlayerMessage.x, createPlayerMessage.y);
				}
				break;

				case GameMessageTypes.RemovePlayer:
				{
					removePlayerMessage.Decode(msg);
					RemovePlayer(removePlayerMessage.id);
				}
				break;

				case GameMessageTypes.UpdatePlayer:
				{
					updatePlayerMessage.Decode(msg);

					int id = updatePlayerMessage.id;
					entityManager.SetShipState(id, updatePlayerMessage.dir, updatePlayerMessage.x, updatePlayerMessage.y, updatePlayerMessage.speed);
				}
				break;

				case GameMessageTypes.CreateBullet:
				{
					CreateBulletMessage message = addBulletList.AddMessage();
					message.Decode(msg);
				}
				break;

				case GameMessageTypes.UpdateBullet:
				{
					UpdateBulletMessage message = updateBulletList.AddMessage();
					message.Decode(msg);
				}
				break;

				case GameMessageTypes.RemoveBullet:
				{
					RemoveBulletMessage message = removeBulletList.AddMessage();
					message.Decode(msg);
				}
				break;

				case GameMessageTypes.DisableShipInvincible:
				{
					disableShipInvincibleMessage.Decode(msg);
					//Do something
					//int id = disableShipInvincibleMessage.id;
				}
				break;
				
				case GameMessageTypes.RespawnShip:
				{
					respawnShipMessage.Decode(msg);
					RespawnShip(respawnShipMessage.id, respawnShipMessage.x, respawnShipMessage.y);
				}
				break;
					
				default: break;
				}

				break;
			}
		}

		entityManager.ProcessNetworkBulletMessages(addBulletList, updateBulletList, removeBulletList);

		addBulletList.Reset();
		updateBulletList.Reset();
		removeBulletList.Reset();
	}

	void OnDestroy()
	{
		client.Shutdown("bye");
	}

	public void StartGame()
	{
		GameState = 3;

		Debug.Log("SessionTicket = " + PlayFabClientAPI.AuthKey);
		
	#if TEST_MODE
		StartGameResult result = new StartGameResult();
		result.ServerHostname = "127.0.0.1";
		result.ServerPort = 14242;		
		result.LobbyID = "1000";
		result.Ticket = "0000";
		StartGameSuccess(result);
	#else
		Matchmake();
		
		/*StartGameRequest request = new StartGameRequest();
		request.BuildVersion = "123";
		request.Region = Region.Singapore;
		request.GameMode = "Normal";
		
		PlayFabClientAPI.StartGame(request, StartGameSuccess, StartGameError);*/
	#endif
	}

	public void Matchmake()
	{
		GameState = 3;
		
		Debug.Log("SessionTicket = " + PlayFabClientAPI.AuthKey);

		MatchmakeRequest request = new MatchmakeRequest();
		request.BuildVersion = "123";
		request.Region = Region.Singapore;
		request.GameMode = "Normal";

		PlayFabClientAPI.Matchmake(request, MatchmakeSuccess, MatchmakeError);
	}

	public void StartGameSuccess(StartGameResult result)
	{
		client.Start();

		string ipAddress = result.ServerHostname;
		int port = result.ServerPort ?? 0;

		Debug.Log("Connect to " + ipAddress + ":" + port.ToString());
		Debug.Log("Lobby ID = " + result.LobbyID); 
		Debug.Log("Ticket = " + result.Ticket); 
		Debug.Log("Expires = " + result.Expires); 

		NetOutgoingMessage approvalMessage = client.CreateMessage();
		approvalMessage.Write(result.Ticket);

		client.Connect(ipAddress, port, approvalMessage);
	}

	public void StartGameError(PlayFabError error)
	{
		Debug.LogError(error.ErrorMessage);
	}

	public void MatchmakeSuccess(MatchmakeResult result)
	{
		client.Start();
		
		string ipAddress = result.ServerHostname;
		int port = result.ServerPort ?? 0;
		
		Debug.Log("Connect to " + ipAddress + ":" + port.ToString());
		Debug.Log("Lobby ID = " + result.LobbyID); 
		Debug.Log("Ticket = " + result.Ticket); 
		Debug.Log("Expires = " + result.Expires); 
		
		NetOutgoingMessage approvalMessage = client.CreateMessage();
		approvalMessage.Write(PlayFabId);
		approvalMessage.Write(result.Ticket);
		
		client.Connect(ipAddress, port, approvalMessage);
	}
	
	public void MatchmakeError(PlayFabError error)
	{
		Debug.LogError(error.ErrorMessage);
	}
	
}
