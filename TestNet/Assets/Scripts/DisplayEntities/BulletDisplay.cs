using UnityEngine;
using System.Collections;

public class BulletDisplay : MonoBehaviour
{
	private Transform cachedTransform;
	
	private Bullet bullet = null;
	
	public Vector3 displayPos;
	
	void Awake()
	{
		cachedTransform = transform;
	}

	public void SetBullet(Bullet bullet)
	{
		this.bullet = bullet;
		displayPos = new Vector3(bullet.x, bullet.y, 0.0f);
		cachedTransform.position = displayPos;
	}
	
	void Update ()
	{
		if (bullet == null) return;
		
		Vector3 newPos = new Vector3(bullet.x, bullet.y, 0.0f);
		displayPos = Vector3.Lerp(displayPos, newPos, 0.1f);

		cachedTransform.position = displayPos;
	}
}

