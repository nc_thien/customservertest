using UnityEngine;
using System.Collections;

public class ShipDisplay : MonoBehaviour
{
	private Transform cachedTransform;

	private Ship ship = null;

	public Vector3 displayPos;
	public Quaternion displayRot;

	private readonly Quaternion baseRot = Quaternion.Euler(90.0f, -180.0f, 0.0f);

	public ParticleSystem[] enginePsList;
	private bool isEngineOn;

	private float dir;

	private float roll;
	private float targetRoll;

	private float rollVel;

	private const float ROLL_LIMIT = 0.4f;
	
	void Awake()
	{
		cachedTransform = transform;
		isEngineOn = true;

		dir = 0.0f;
		roll = 0.0f;
	}

	public void SetShip(Ship ship)
	{
		this.ship = ship;

		displayPos = new Vector3(ship.x, ship.y, 0.0f);
		cachedTransform.position = displayPos;

		displayRot = Quaternion.Euler(0.0f, 0.0f, -Mathf.Rad2Deg * ship.dir) * baseRot;
		cachedTransform.rotation = displayRot;

		StopEngine();
	}

	public void StartEngine()
	{
		if (isEngineOn) return;
		isEngineOn = true;

		for (int i = 0; i < enginePsList.Length; ++i) enginePsList[i].enableEmission = true;
	}

	public void StopEngine()
	{
		if (!isEngineOn) return;
		isEngineOn = false;

		for (int i = 0; i < enginePsList.Length; ++i) enginePsList[i].enableEmission = false;
	}
	
	void Update ()
	{
		if (ship == null) return;

		if (ship.speed > 0.0f) StartEngine();
		else StopEngine();

		Vector3 newPos = new Vector3(ship.x, ship.y, 0.0f);
		displayPos = Vector3.Lerp(displayPos, newPos, 0.1f);

		float newDir = Mathf.LerpAngle(dir * Mathf.Rad2Deg, ship.dir * Mathf.Rad2Deg, 0.1f) * Mathf.Deg2Rad;

		float rotation = newDir - dir;
		if (Mathf.Abs(rotation) > Mathf.PI)
		{
			if (rotation < 0.0f) rotation += Mathf.PI * 2.0f;
			else rotation -= Mathf.PI * 2.0f;
		}

		if (rotation < -0.01f) targetRoll = -ROLL_LIMIT;
		else if (rotation > 0.01f) targetRoll = ROLL_LIMIT;
		else targetRoll = 0.0f;

		roll = Mathf.SmoothDamp(roll, targetRoll, ref rollVel, 0.5f);

		dir = newDir;
		displayRot = Quaternion.Euler(0.0f, 0.0f, -Mathf.Rad2Deg * dir) * baseRot * Quaternion.Euler(0.0f, 0.0f, Mathf.Rad2Deg * roll);

		cachedTransform.rotation = displayRot;
		cachedTransform.position = displayPos;
	}
}

