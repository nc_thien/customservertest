using Lidgren.Network;

public class RespawnShipMessage : GameMessage
{
	public int id;
	public float x;
	public float y;
	
	public RespawnShipMessage()
	{
		type = GameMessageTypes.RespawnShip;
	}
	
	public override void Decode(NetIncomingMessage im)
	{
		id = im.ReadInt32();
		x = im.ReadFloat();
		y = im.ReadFloat();
	}
	
	public override void Encode(NetOutgoingMessage om)
	{
		om.Write(id);
		om.Write(x);
		om.Write(y);
	}
}