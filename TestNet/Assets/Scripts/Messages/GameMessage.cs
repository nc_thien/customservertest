﻿using Lidgren.Network;

public abstract class GameMessage
{
    public GameMessageTypes type;

    public abstract void Decode(NetIncomingMessage im);
	public abstract void Encode(NetOutgoingMessage om);

	public void Send(NetPeer peer, NetConnection recipient, NetDeliveryMethod method)
	{
		NetOutgoingMessage om = peer.CreateMessage();
		om.Write((byte)type);
		Encode(om);
		
		peer.SendMessage(om, recipient, method);
	}

	public void Send(NetClient client, NetDeliveryMethod method)
	{
		NetOutgoingMessage om = client.CreateMessage();
		om.Write((byte)type);
		Encode(om);
		
		client.SendMessage(om, method);
	}

	public void SendAll(NetServer server, NetDeliveryMethod method)
	{
		NetOutgoingMessage om = server.CreateMessage();
		om.Write((byte)type);
		Encode(om);

		server.SendToAll(om, method);
	}

    public void SendAll(NetServer server, NetConnection except, NetDeliveryMethod method)
    {
        NetOutgoingMessage om = server.CreateMessage();
        om.Write((byte)type);
        Encode(om);

        server.SendToAll(om, except, method, 0);
    }
}