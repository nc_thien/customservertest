﻿using Lidgren.Network;

public class CreatePlayerMessage : GameMessage
{
	public int id;
    public bool isLocal;

	public float x;
	public float y;

	public CreatePlayerMessage()
    {
		type = GameMessageTypes.CreatePlayer;
    }

	public override void Decode(NetIncomingMessage im)
    {
		id = im.ReadInt32();
        isLocal = im.ReadBoolean();
		x = im.ReadFloat();
		y = im.ReadFloat();
    }

	public override void Encode(NetOutgoingMessage om)
    {
        om.Write(id);
        om.Write(isLocal);
		om.Write(x);
		om.Write(y);
    }
}