using Lidgren.Network;

public class UpdatePlayerInputMessage : GameMessage
{
	public int id;
	public float h;
	public float v;
	
	public UpdatePlayerInputMessage()
	{
		type = GameMessageTypes.UpdatePlayerInput;
	}
	
	public override void Decode(NetIncomingMessage im)
	{
		id = im.ReadInt32();
		h = im.ReadFloat();
		v = im.ReadFloat();
	}
	
	public override void Encode(NetOutgoingMessage om)
	{
		om.Write(id);
		om.Write(h);
		om.Write(v);
	}
}