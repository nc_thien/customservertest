using Lidgren.Network;

public class RemovePlayerMessage : GameMessage
{
	public int id;
	
	public RemovePlayerMessage()
	{
		type = GameMessageTypes.RemovePlayer;
	}
	
	public override void Decode(NetIncomingMessage im)
	{
		id = im.ReadInt32();
	}
	
	public override void Encode(NetOutgoingMessage om)
	{
		om.Write(id);
	}
}