using System.Collections.Generic;

public class GameMessageList<T> where T : GameMessage, new()
{
	private List<T> items;
	private int count;

	public GameMessageList()
	{
		items = new List<T>();
		count = 0;
	}

	public T AddMessage()
	{
		if (count < items.Count)
		{
			return items[count++];
		}
		else 
		{
			T item = new T();
			items.Add(item);

			count++;

			return item;
		}
	}

	public void Reset()
	{
		count = 0;
	}

	public void Sort()
	{
		if (count > 0)
		{
			items.Sort(0, count, null);
		}
	}

	public int GetCount()
	{
		return count;
	}

	public T GetItem(int pos)
	{
		if (pos < 0 || pos >= count) return null;
		return items[pos];
	}
}