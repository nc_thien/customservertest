using System;
using System.Collections;
using Lidgren.Network;

public class RemoveBulletMessage : GameMessage, IComparable<RemoveBulletMessage>
{
	public int id;
	
	public RemoveBulletMessage()
	{
		type = GameMessageTypes.RemoveBullet;
	}
	
	public override void Decode(NetIncomingMessage im)
	{
		id = im.ReadInt32();
	}
	
	public override void Encode(NetOutgoingMessage om)
	{
		om.Write(id);
	}

	public int CompareTo(RemoveBulletMessage other)
	{
		return id.CompareTo(other.id);
	}
}

