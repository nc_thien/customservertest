using Lidgren.Network;

public class PressFireMessage : GameMessage
{
	public int id;
	public bool isPressed;
	
	public PressFireMessage()
	{
		type = GameMessageTypes.PressFire;
	}
	
	public override void Decode(NetIncomingMessage im)
	{
		id = im.ReadInt32();
		isPressed = im.ReadBoolean();
	}
	
	public override void Encode(NetOutgoingMessage om)
	{
		om.Write(id);
		om.Write(isPressed);
	}
}