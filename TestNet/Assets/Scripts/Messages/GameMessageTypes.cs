﻿public enum GameMessageTypes
{
	CreatePlayer = 0,
	RemovePlayer,
	UpdatePlayerInput,
	PressFire,
	UpdatePlayer,
	CreateBullet,
	RemoveBullet,
	UpdateBullet,
	DisableShipInvincible,
	RespawnShip,
}