using System;
using System.Collections;
using Lidgren.Network;

public class CreateBulletMessage : GameMessage, IComparable<CreateBulletMessage>
{
	public int id;
	public int shipId;
	public float x;
	public float y;
	public float dir;

	public CreateBulletMessage()
	{
		type = GameMessageTypes.CreateBullet;
	}

	public override void Decode(NetIncomingMessage im)
	{
		id = im.ReadInt32();
		shipId = im.ReadInt32();
		x = im.ReadFloat();
		y = im.ReadFloat();
		dir = im.ReadFloat();
	}
	
	public override void Encode(NetOutgoingMessage om)
	{
		om.Write(id);
		om.Write(shipId);
		om.Write(x);
		om.Write(y);
		om.Write(dir);
	}

	public int CompareTo(CreateBulletMessage other)
	{
		return id.CompareTo(other.id);
	}
}

