using Lidgren.Network;

public class DisableShipInvincibleMessage : GameMessage
{
	public int id;
	
	public DisableShipInvincibleMessage()
	{
		type = GameMessageTypes.DisableShipInvincible;
	}
	
	public override void Decode(NetIncomingMessage im)
	{
		id = im.ReadInt32();
	}
	
	public override void Encode(NetOutgoingMessage om)
	{
		om.Write(id);
	}
}