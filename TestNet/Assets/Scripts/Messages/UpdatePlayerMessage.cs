using Lidgren.Network;

public class UpdatePlayerMessage : GameMessage
{
	public int id;
	public float x;
	public float y;
	public float dir;
	public float speed;
	
	public UpdatePlayerMessage()
	{
		type = GameMessageTypes.UpdatePlayer;
	}
	
	public override void Decode(NetIncomingMessage im)
	{
		id = im.ReadInt32();
		x = im.ReadFloat();
		y = im.ReadFloat();
		dir = im.ReadFloat();
		speed = im.ReadFloat();
	}
	
	public override void Encode(NetOutgoingMessage om)
	{
		om.Write(id);
		om.Write(x);
		om.Write(y);
		om.Write(dir);
		om.Write(speed);
	}
}