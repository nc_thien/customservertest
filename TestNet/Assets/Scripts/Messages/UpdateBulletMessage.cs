using System;
using System.Collections;
using Lidgren.Network;

public class UpdateBulletMessage : GameMessage, IComparable<UpdateBulletMessage>
{
	public int id;
	public float x;
	public float y;
		
	public UpdateBulletMessage()
	{
		type = GameMessageTypes.UpdateBullet;
	}
	
	public override void Decode(NetIncomingMessage im)
	{
		id = im.ReadInt32();
		x = im.ReadFloat();
		y = im.ReadFloat();
	}
	
	public override void Encode(NetOutgoingMessage om)
	{
		om.Write(id);
		om.Write(x);
		om.Write(y);
	}

	public int CompareTo(UpdateBulletMessage other)
	{
		return id.CompareTo(other.id);
	}
}

