using System;
using System.Collections.Generic;

using Lidgren.Network;

public class EntityManager
{
	public delegate void CreateBulletHandler(Bullet bullet);
	public CreateBulletHandler OnCreatBullet;

	public delegate void RemoveBulletHandler(Bullet bullet);
	public RemoveBulletHandler OnRemoveBullet;

	public float boundMinX, boundMaxX;
	public float boundMinY, boundMaxY;

	private bool isServer = true;

	public int localId = -1;

	public List<Ship> ships;

	public List<Bullet> bullets;
	public List<Bullet> bulletBufs;

    private CreatePlayerMessage createPlayerMessage = null;
    private UpdatePlayerMessage updatePlayerMessage = null;
    
	private RemoveBulletMessage removeBulletMessage = null;
	private CreateBulletMessage createBulletMessage = null;
	private UpdateBulletMessage updateBulletMessage = null;

	private DisableShipInvincibleMessage disableShipInvincibleMessage = null;
	private RespawnShipMessage respawnShipMessage = null;

    private int globalBulletId;

	private NetServer server = null;
	private NetClient client = null;

    private const float SHIP_SEND_TIME = 1.0f / 10.0f;
    private const float BULLET_SEND_TIME = 1.0f / 5.0f;

    private float shipSendTimer;
    private float bulletSendTimer;

	private Random random = null;

	public EntityManager(NetPeer peer, bool isServer = true)
	{
		this.isServer = isServer;

		if (isServer)
		{
			server = (NetServer)peer;
		}
		else 
		{
			client = (NetClient)peer;
		}

		ships = new List<Ship>();

		bullets = new List<Bullet>();
		bulletBufs = new List<Bullet>();
	
		boundMinX = Constants.BOUND_MIN_X;
		boundMinY = Constants.BOUND_MIN_Y;

		boundMaxX = Constants.BOUND_MAX_X;
		boundMaxY = Constants.BOUND_MAX_Y;

        globalBulletId = 0;

        shipSendTimer = 0.0f;
        bulletSendTimer = 0.0f;

		if (isServer)
		{
			random = new Random();

            createPlayerMessage = new CreatePlayerMessage();
            updatePlayerMessage = new UpdatePlayerMessage();
            
			removeBulletMessage = new RemoveBulletMessage();
			createBulletMessage = new CreateBulletMessage();
			updateBulletMessage = new UpdateBulletMessage();

			disableShipInvincibleMessage = new DisableShipInvincibleMessage();
			respawnShipMessage = new RespawnShipMessage();
		}
	}

    public Ship CreateShip()
    {
        float x, y;
        GetRandomPosition(out x, out y);

        return CreateShip(x, y);
    }

	public Ship CreateShip(float x, float y)
	{
		Ship ship = new Ship();

		ship.x = x;
		ship.y = y;

		ship.radius = Constants.SHIP_RADIUS;
		ship.rotSpeed = Constants.SHIP_ROT_SPEED;
		ship.maxSpeed = Constants.SHIP_MAX_SPEED;
		ship.accel = Constants.SHIP_ACCEL;
        ship.decel = Constants.SHIP_DECEL;

		return ship;
	}

	public Bullet CreateBullet(int id, int shipId, float x, float y, float dir)
	{
		Bullet bullet = new Bullet(id, shipId, x, y, dir);
        bullet.speed = Constants.BULLET_SPEED;
        bullet.radius = Constants.BULLET_RADIUS;

		return bullet;
	}

	public void SetShip(Ship ship, int id)
	{
		int numShips = ships.Count;
		while (id >= numShips)
		{
			ships.Add(null);
			numShips++;
		}
			
		ships[id] = ship;
	}

	public void RemoveShip(int id)
	{
		int numShips = ships.Count;
		if (id < numShips) ships[id] = null;
	}

	public Ship GetShip(int id)
	{
		int numShips = ships.Count;
		if (id < numShips) return ships[id];
		else return null;
	}

	public void GetRandomPosition(out float x, out float y)
	{
		x = ((float)random.NextDouble()) * (boundMaxX - boundMinX) + boundMinX;
		y = ((float)random.NextDouble()) * (boundMaxY - boundMinY) + boundMinY;
	}

	public void Update(float dt)
	{
		int numShips = ships.Count;
		for (int i = 0; i < numShips; ++i)
		{
			Ship ship = ships[i];
			if (ship != null)	
			{
				ship.Update(dt);
				ship.CheckBound(boundMinX, boundMinY, boundMaxX, boundMaxY);
			}
		}

		int numBullets = bullets.Count;
		for (int i = 0; i < numBullets; ++i)
		{
			Bullet bullet = bullets[i];
			bullet.Update(dt);
			bullet.CheckBound(boundMinX, boundMinY, boundMaxX, boundMaxY);
		}

		if (isServer)
		{
            bool needSendUpdate = false;

            shipSendTimer -= dt;
            if (shipSendTimer <= 0.0f) needSendUpdate = true;

            for (int i = 0; i < numShips; ++i)
            {
                Ship ship = ships[i];

                if (ship != null)
                {
                    if (ship.fireFlag) needSendUpdate = true;

                    if (needSendUpdate)
                    {
                        shipSendTimer = SHIP_SEND_TIME;

                        updatePlayerMessage.id = i;

                        updatePlayerMessage.x = ship.x;
                        updatePlayerMessage.y = ship.y;

                        updatePlayerMessage.dir = ship.dir;
                        updatePlayerMessage.speed = ship.speed;

                        updatePlayerMessage.SendAll(server, NetDeliveryMethod.UnreliableSequenced);
                    }

                    if (ship.fireFlag)
                    {
                        ship.fireFlag = false;

                        Bullet bullet = CreateBullet(globalBulletId++, i, ship.x, ship.y, ship.dir);
                        bullets.Add(bullet);

                        createBulletMessage.id = bullet.id;
                        createBulletMessage.shipId = bullet.shipId;
                        createBulletMessage.x = bullet.x;
                        createBulletMessage.y = bullet.y;
                        createBulletMessage.dir = ship.dir;
                        createBulletMessage.SendAll(server, NetDeliveryMethod.ReliableOrdered);
                    }

					if (ship.disableInvincibleFlag)
					{
						ship.disableInvincibleFlag = false;

						disableShipInvincibleMessage.id = i;
						disableShipInvincibleMessage.SendAll(server, NetDeliveryMethod.ReliableOrdered);
					}
                }
            }

            CheckShipShipCollision();
            CheckShipBulletCollision();

            needSendUpdate = false;

            bulletSendTimer -= dt;
            if (bulletSendTimer <= 0.0f) needSendUpdate = true;

            int pos = 0;

			for (int i = 0; i < numBullets; ++i)
			{
				Bullet bullet = bullets[i];

				if (!bullet.isActive)
				{
					removeBulletMessage.id = bullet.id;
					removeBulletMessage.SendAll(server, NetDeliveryMethod.ReliableOrdered);
				}
				else 
				{
                    if (needSendUpdate)
                    {
                        bulletSendTimer = BULLET_SEND_TIME;

                        updateBulletMessage.id = bullet.id;
                        updateBulletMessage.x = bullet.x;
                        updateBulletMessage.y = bullet.y;
						updateBulletMessage.SendAll(server, NetDeliveryMethod.UnreliableSequenced);
                    }

                    bullets[pos++] = bullet;
				}
			}

            if (pos < numBullets)
            {
                bullets.RemoveRange(pos, numBullets - pos);
            }

			for (int i = 0; i < numShips; ++i)
			{
				Ship ship = ships[i];
				if (ship == null) continue;
				
				if (!ship.isActive)
				{
					ship.StartInvincible();

					ship.isActive = true;
					GetRandomPosition(out ship.x, out ship.y);

                    ship.speed = 0.0f;

					respawnShipMessage.id = i;
					respawnShipMessage.x = ship.x;
					respawnShipMessage.y = ship.y;
					respawnShipMessage.SendAll(server, NetDeliveryMethod.ReliableOrdered);						
				}
			}
		}
	}

    public void CheckShipShipCollision()
    {
        int numShips = ships.Count;

        for (int i = 0; i < numShips; ++i)
        {
            Ship ship = ships[i];
            if (ship == null) continue;

            for (int j = i + 1; j < numShips; ++j)
            {
                Ship ship2 = ships[j];
                if (ship2 == null) continue;

                if (ship.Collide(ship2))
                {
                    ship.isActive = false;
                    ship2.isActive = false;
                }
            }
        }
    }

    public void CheckShipBulletCollision()
    {
        int numBullets = bullets.Count;
        int numShips = ships.Count;

        for (int i = 0; i < numBullets; ++i)
        {
            Bullet bullet = bullets[i];

            for (int j = 0; j < numShips; ++j)
		    {
                if (j == bullet.shipId) continue;

			    Ship ship = ships[j];
                if (ship != null)
                {
                    if (ship.Collide(bullet))
                    {
                        bullet.isActive = false;
                        ship.isActive = false;
                    }
                }
            }
        }
    }

    public void InitWorldState(NetConnection connection)
    {
        int numShips = ships.Count;
        for (int i = 0; i < numShips; ++i)
        {
            Ship ship = ships[i];
            if (ship == null) continue;

            createPlayerMessage.id = i;
            createPlayerMessage.isLocal = false;
            createPlayerMessage.x = ship.x;
            createPlayerMessage.y = ship.y;
            createPlayerMessage.Send(server, connection, NetDeliveryMethod.ReliableOrdered);
        }

        int numBullets = bullets.Count;
        for (int i = 0; i < numBullets; ++i)
        {
            Bullet bullet = bullets[i];

            createBulletMessage.id = bullet.id;
            createBulletMessage.shipId = bullet.shipId;
            createBulletMessage.x = bullet.x;
            createBulletMessage.y = bullet.y;
            createBulletMessage.dir = bullet.dir;
            createBulletMessage.Send(server, connection, NetDeliveryMethod.ReliableOrdered);
        }
    }

    public void ProcessNetworkBulletMessages(GameMessageList<CreateBulletMessage> addList, GameMessageList<UpdateBulletMessage> updateList, GameMessageList<RemoveBulletMessage> removeList)
	{
		addList.Sort();
		updateList.Sort();
		removeList.Sort();

		int updateListCount = updateList.GetCount();
		int addListCount = addList.GetCount();
		int removeListCount = removeList.GetCount();

		int updateIndex = 0;
		int addIndex = 0;
		int removeIndex = 0;

		int numBullets = bullets.Count;

		bulletBufs.Clear();

		for (int i = 0; i < numBullets; ++i)
		{
			Bullet bullet = bullets[i];
			int id = bullet.id;

			while (addIndex < addListCount)
			{
				CreateBulletMessage message = addList.GetItem(addIndex);
				int addId = message.id;

				if (addId < id)
				{
					bool validAdd = true;

					while (removeIndex < removeListCount)
					{
						RemoveBulletMessage removeMessage = removeList.GetItem(removeIndex);
						int removeId = removeMessage.id;

						if (removeId == addId)
						{
							validAdd = false;

							removeIndex++;
							break;
						}
						else if (removeId > addId) break;

						removeIndex++;
					}

					if (validAdd)
					{
						Bullet newBullet = CreateBullet(message.id, message.shipId, message.x, message.y, message.dir);
						bulletBufs.Add(newBullet);

						if (OnCreatBullet != null) OnCreatBullet(newBullet);
					}
				}
				else if (addId > id) break;

				addIndex++;
			}

			bool valid = true;

			while (removeIndex < removeListCount)
			{
				RemoveBulletMessage message = removeList.GetItem(removeIndex);
				int removeId = message.id;

				if (removeId == id)
				{
					valid = false;

					if (OnRemoveBullet != null) OnRemoveBullet(bullet);

					removeIndex++;
					break;
				}
				else if (removeId > id) break;

				removeIndex++;
			}

			if (!valid) continue;

			while (updateIndex < updateListCount)
			{
				UpdateBulletMessage message = updateList.GetItem(updateIndex);
				int updateId = message.id;

				if (updateId == id)
				{
					bullet.SetState(message.x, message.y);
					updateIndex++;
					break;
				}
				else if (updateId > id) break;

				updateIndex++;
			}

			bulletBufs.Add(bullet);
		}

		while (addIndex < addListCount)
		{
			CreateBulletMessage message = addList.GetItem(addIndex);
			int addId = message.id;
			
			bool validAdd = true;
				
			while (removeIndex < removeListCount)
			{
				RemoveBulletMessage removeMessage = removeList.GetItem(removeIndex);
				int removeId = removeMessage.id;
				
				if (removeId == addId) 
				{
					validAdd = false;

					removeIndex++;
					break;
				}
				else if (removeId > addId) break;
				
				removeIndex++;
			}
			
			if (validAdd)
			{
				Bullet newBullet = CreateBullet(message.id, message.shipId, message.x, message.y, message.dir);
				bulletBufs.Add(newBullet);

				if (OnCreatBullet != null) OnCreatBullet(newBullet);
			}
			
			addIndex++;
		}

		bullets.Clear();
		List<Bullet> tmp = bullets;
		bullets = bulletBufs;
		bulletBufs = tmp;
	}

	public void UpdateShipInput(int id, float h, float v)
	{
		int numShips = ships.Count;
		if (id < numShips)
		{
			Ship ship = ships[id];
			if (ship != null)
			{
				ship.UpdateInput(h, v);
			}
		}
	}

    public void PressFire(int id, bool isPressed)
    {
        int numShips = ships.Count;
        if (id < numShips)
        {
            Ship ship = ships[id];
            if (ship != null)
            {
                ship.PressFire(isPressed);
            }
        }
    }

	public void SetShipState(int id, float dir, float x, float y, float speed)
	{
		int numShips = ships.Count;
		if (id < numShips)
		{
			Ship ship = ships[id];
			if (ship != null)
			{
				ship.SetState(dir, x, y, speed);
 			}
		}
	}
}

