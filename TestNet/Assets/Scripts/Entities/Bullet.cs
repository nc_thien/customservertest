using System;

public class Bullet
{
	public int id;
	public int shipId;

    public float dir;

	public float x, y;
	private float vx, vy;

    public float speed;
    public float radius;

	public bool isActive;

	public object tag = null;

	public Bullet(int id, int shipId, float x, float y, float dir)
	{
		isActive = true;

        this.dir = dir;

		this.id = id;
		this.shipId = shipId;

		this.x = x;
		this.y = y;

        vx = (float)Math.Sin(dir);
        vy = (float)Math.Cos(dir);
	}

	public void SetState(float x, float y)
	{
		this.x = x;
		this.y = y;
	}
	
	public void Update(float dt)
	{
		x += dt * vx * speed;
		y += dt * vy * speed;
	}

	public void CheckBound(float minX, float minY, float maxX, float maxY)
	{
        if (x < minX || x > maxX || y < minY || y > maxY) isActive = false;
	}
}

