public static class Constants
{
	public const float BOUND_MIN_X = -40.0f;
	public const float BOUND_MAX_X = 40.0f;

	public const float BOUND_MIN_Y = -40.0f;
	public const float BOUND_MAX_Y = 40.0f;

	public const float SHIP_MAX_SPEED = 16.0f;
	public const float SHIP_ACCEL = 8.0f;
    public const float SHIP_DECEL = 4.0f;
	public const float SHIP_ROT_SPEED = 3.0f;
	public const float SHIP_RADIUS = 1.0f;

    public const float SHIP_FIRE_COUNTDOWN = 0.3f;
    public const float SHIP_RECOIL_SPEED = 2.0f;

    public const float SHIP_INVINCIBLE_TIME = 2.0f;
        
	public const float BULLET_SPEED = 24.0f;
    public const float BULLET_RADIUS = 0.2f;
}

