using System;

public class Ship
{
	public float x, y;
	public float dir;

	private float vx, vy;

	public float radius;

	public float rotSpeed;
	public float maxSpeed;
	public float accel;
    public float decel;

	public float speed;

	public object tag = null;

    public bool isActive;

    public bool pressFire = false;
    public float fireCountdown = 0.0f;
    public bool fireFlag = false;

    public bool isInvincible = false;
    public float invincibleCountdown = 0.0f;
    public bool disableInvincibleFlag = false;

	public Ship()
	{
        isActive = true;

		dir = 0.0f;
		UpdateDir();

		speed = 0.0f;

        pressFire = false;
        fireCountdown = 0.0f;
        fireFlag = false;

        isInvincible = false;
        invincibleCountdown = 0.0f;
        disableInvincibleFlag = false;
	}

    public void StartInvincible()
    {
        isInvincible = true;
        invincibleCountdown = Constants.SHIP_INVINCIBLE_TIME;
    }

    public bool Collide(Bullet bullet)
    {
        return (bullet.x - x) * (bullet.x - x) + (bullet.y - y) * (bullet.y - y) <= (radius + bullet.radius) * (radius + bullet.radius);
    }

    public bool Collide(Ship ship)
    {
        return (ship.x - x) * (ship.x - x) + (ship.y - y) * (ship.y - y) <= (radius + ship.radius) * (radius + ship.radius);
    }

	private void UpdateDir()
	{
		vx = (float)Math.Sin(dir);
		vy = (float)Math.Cos(dir);
	}

	public void UpdateInput(float h, float v)
	{
		dir += h * rotSpeed;

		dir %= 2.0f * (float)Math.PI;
		if (dir < 0.0f) dir += 2.0f * (float)Math.PI;

		UpdateDir();

        if (v >= 0.0f) speed += v * accel;
        else speed += v * decel;

		if (speed < 0.0f) speed = 0.0f;
		else if (speed > maxSpeed) speed = maxSpeed;
	}

    public void PressFire(bool isPressed)
    {
        pressFire = isPressed;
    }

	public void SetState(float dir, float x, float y, float speed)
	{
		this.dir = dir;
		UpdateDir();

		this.x = x;
		this.y = y;

		this.speed = speed;
	}

	public void Update(float dt)
	{
		x += dt * vx * speed;
		y += dt * vy * speed;

        if (isInvincible)
        {
            invincibleCountdown -= dt;
            if (invincibleCountdown <= 0.0f)
            {
                invincibleCountdown = 0.0f;
                isInvincible = false;

                disableInvincibleFlag = true;
            }
        }

        if (fireCountdown > 0.0f)
        {
            fireCountdown -= dt;
            if (fireCountdown < 0.0f) fireCountdown = 0.0f;
        }

        if (pressFire)
        {
            if (fireCountdown <= 0.0f)
            {
                speed -= Constants.SHIP_RECOIL_SPEED;
                if (speed < 0.0f) speed = 0.0f;

                fireFlag = true;
                fireCountdown = Constants.SHIP_FIRE_COUNTDOWN;
            }
        }
	}

	public void CheckBound(float minX, float minY, float maxX, float maxY)
	{
        if (x < minX || x > maxX || y < minY || y > maxY) isActive = false;
	}
}

